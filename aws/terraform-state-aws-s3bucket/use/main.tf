provider "aws" {
  region = "ap-southeast-2"
}

terraform {
  backend "s3" {
    bucket         = "aaron-terraform-state-fridaythenineteenth"
    key            = "global/s3/terraform.tfstate"
    region         = "ap-southeast-2"

    dynamodb_table = "aaron-terraform-state-lock-fridaythenineteenth"
    encrypt        = true
  }
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "aaron-terraform-state-fridaythenineteenth"

  lifecycle {
    prevent_destroy = true
  }
  
  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}