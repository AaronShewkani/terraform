# INIT

terraform {
  # tf cloud for state mgmt
  # backend "remote" {
  #   organization = "theflash"
  #   workspaces {
  #     name = "Example-Workspace"
  #   }
  # }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

# ------------- BUILD -------------

resource "aws_instance" "example" {
  ami           = "ami-075a72b1992cb0687" # Amazon Linux 2 AMI (HVM), SSD Volume Type in AP-SOUTHEAST-2
  instance_type = var.instanceType

  tags = {
    Name = "ExampleInstance"
  }
}

# ------------- CHANGE -------------

# resource "aws_instance" "example" {
#   ami           = "ami-076a5bf4a712000ed" # Ubuntu, SSD Volume Type in AP-SOUTHEAST-2
#   instance_type = var.instanceType

#   tags = {
#     Name = "ExampleInstance"
#   }
# }

# ------------- DESTROY -------------

# Run terraform destroy - will destroy whatevers in the state (terraform state list)

# ------------- DEFINE INPUT VARS -------------

# New file variables.tf loads the vars 
# New file terraform.tfvars - prop value file
# New file sensitive.tfvars - prop value file

# Add from CLI: terraform apply -var="region=us-east-1"

# Use terraform.tfvars file (must be named this) or sensitive.tfvars (load using -var-file="sensitive.tfvars")

# ------------- OUTPUT VARS AND QUERYING -------------

# New file outputs.tf - prop value file

