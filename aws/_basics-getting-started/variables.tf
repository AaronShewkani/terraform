# Direct define
# variable "region" {
#     default = "ap-southeast-2"
# }

variable "region" {}
variable "instanceType" {
  default = "t2.micro"
}
variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "ExampleInstance"
}