provider "aws" {
  region = "ap-southeast-2"
}

terraform {
  backend "s3" {
    bucket         = "aaron-terraform-state-fridaythenineteenth"
    key            = "workspaces-example/terraform.tfstate"
    region         = "ap-southeast-2"

    dynamodb_table = "aaron-terraform-state-lock-fridaythenineteenth"
    encrypt        = true
  }
}

resource "aws_instance" "example" {
  ami           = "ami-075a72b1992cb0687"
  instance_type = "t2.micro"
}

# terraform workspace show
# terraform workspace list
# terraform workspace new dev
# terraform workspace new prod
# terraform workspace select dev

resource "aws_instance" "example" {
  ami           = "ami-075a72b1992cb0687"
  instance_type = terraform.workspace == "prod" ? "t2.medium" : "t2.micro"
}