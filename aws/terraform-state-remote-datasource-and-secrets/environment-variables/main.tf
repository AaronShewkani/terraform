provider "aws" {
  region = "ap-southeast-2"
}

terraform {
  backend "s3" {
    bucket         = "aaron-terraform-state-fridaythenineteenth"
    key            = "stage/data-stores/mysql/terraform.tfstate"
    region         = "ap-southeast-2"

    dynamodb_table = "aaron-terraform-state-lock-fridaythenineteenth"
    encrypt        = true
  }
}

# SECRETs can be loaded from env - e.g. powershell / jenkins / bambook
variable "db_password" {
  description = "The password for the database"
  type        = string
}
# Load it like this in cli (ps for example)
# set the env var TF_VAR_db_password="pwd" then do terraform apply

resource "aws_db_instance" "example" {
  identifier_prefix   = "instance-aaron"
  engine              = "mysql"
  allocated_storage   = 10
  instance_class      = "db.t2.micro"
  name                = "example_database"
  username            = "admin"

  password = var.db_password
}

output "address" {
  value       = aws_db_instance.example.address
  description = "Connect to the database at this endpoint"
}

output "port" {
  value       = aws_db_instance.example.port
  description = "The port the database is listening on"
}