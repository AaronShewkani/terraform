provider "aws" {
  region = "ap-southeast-2"
}

# SECRET MGNRS ALLOWED
# AWS Secrets Manager and the aws_secretsmanager_secret_version data source (shown in the preceding code)
# AWS Systems Manager Parameter Store and the aws_ssm_parameter data source
# AWS Key Management Service (AWS KMS) and the aws_kms_secrets data source
# Google Cloud KMS and the google_kms_secret data source
# Azure Key Vault and the azurerm_key_vault_secret data source
# HashiCorp Vault and the vault_generic_secret data source

resource "aws_db_instance" "example" {
  identifier_prefix   = "instance-aaron"
  engine              = "mysql"
  allocated_storage   = 10
  instance_class      = "db.t2.micro"
  name                = "example_database"
  username            = "admin"

  password = data.whateversecretmgnr.db_password.secret_string
}

